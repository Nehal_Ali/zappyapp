from django.contrib import admin
from .models import Person, Tweets

admin.site.register(Tweets)
admin.site.register(Person)
