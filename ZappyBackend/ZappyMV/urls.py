from django.urls import path

from . import views

urlpatterns = [
    path(r'', views.index, name='index'),
    path('<int:tweet_id>/', views.detail, name='detail'),
    path('<int:tweet_id>/results/', views.results, name='results'),
    #path('<int:tweet_id>/username/', views.username, name='username'),
    #path(r'',views.home_timeline, name='home_timeline')

]
