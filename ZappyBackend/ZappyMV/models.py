from django.db import models

class Person(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    def __str__(self):
        return self.first_name

class Tweets(models.Model):
    content = models.CharField(max_length=280)
    #date = models.DateField(max_length=30)
    def __str__(self):
        return self.content

class TestingIt(models.Model):
    ay7aga = models.CharField(max_length=90)
