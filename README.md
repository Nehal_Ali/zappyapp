Zappy is a tool that listens to messages on Slack and retrieves tweets to store in MongoDB then viewed by the user

# Requirements

- Python 3.7
- Django 2.1.7
- Tweepy api
- MongoDB djongo
- django-slack api
- angular 4
- node.js
- npm
- pip

# Required Basic Information

- Twitter username: @FictionFoneFS
- Twitter Password: fullstack123
- Slack Workspace: zappyslack.slack.com
- Slack User e-mail: fullstackZappy@gmail.com
- Slack Password: fullstack123
- Slack Channel messages: https://zappyslack.slack.com/messages/CGXDZ4NDV/

# HOW TO RUN

- Pull repository
- Open project via desired IDE
- Initiale virtual environment for project
- Manage.py runserver
-- This will run on port 8000
-- https://127.0.0.1:8000/ZappyMV
-- You can also run the project's views.py in IDE to see tweets
-- Long in to Django's admin page to view database entries being saved

# Scenario for Slack's Events API

- Open rngrok to tunnel server to local host to use Slack's channels.history API
!! This part was not integrated to existing project

